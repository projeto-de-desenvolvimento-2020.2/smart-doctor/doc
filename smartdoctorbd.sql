﻿-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 07-Maio-2020 às 22:19
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `smartdoctor2020`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `agenda`
--

CREATE TABLE `agenda` (
  `cdagen` bigint(20) NOT NULL,
  `cdusua` varchar(14) DEFAULT NULL,
  `cdmedi` varchar(14) DEFAULT NULL,
  `cdform` char(2) DEFAULT NULL,
  `cdplan` char(2) DEFAULT NULL,
  `deespe` varchar(100) DEFAULT NULL,
  `dedeta` varchar(500) DEFAULT NULL,
  `dtagen` datetime DEFAULT NULL,
  `vlcons` decimal(18,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `agenda`
--

INSERT INTO `agenda` (`cdagen`, `cdusua`, `cdmedi`, `cdform`, `cdplan`, `deespe`, `dedeta`, `dtagen`, `vlcons`) VALUES
(1, '132452567', '00000000474', 'CC', '01', 'Clinica Geral', 'Sente dores nas costas', '2020-05-07 15:30:15', '87.00'),
(2, '111111111111', '00000000474', 'CD', '01', 'Oftalmo', 'Sente dores nos olhos e lacrimejam muito', '2020-05-07 12:13:07', '125.00'),
(3, '315426378', '00000000474', 'DN', '02', 'Endocrino', 'Falta de apetite', '2020-05-07 09:53:47', '50.00'),
(4, '27145315807', '00000000474', 'PS', '02', 'Otorrino', 'Sente dores no ouvido', '2020-05-07 11:53:47', '20.00'),
(5, '27145315807', '00000000474', 'CC', '02', 'Exames', 'Sente dores nas pernas', '2020-05-07 13:53:47', '87.00'),
(6, '4235246673', '44171378540', 'DN', '02', 'Ortopedia', '', '2020-05-07 15:33:10', '350.00'),
(7, '49256742934', '44171378540', 'DN', '01', 'Pediatria', '', '2020-05-07 14:59:55', '300.00'),
(8, '00000000373', '00000000474', 'CC', '02', 'Otorinolaringologista', 'Otorrinolaringologista', '2020-05-07 11:00:00', '256.77');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contas`
--

CREATE TABLE `contas` (
  `cdcont` bigint(20) NOT NULL,
  `decont` varchar(50) DEFAULT NULL,
  `dtcont` date DEFAULT NULL,
  `vlcont` decimal(15,2) DEFAULT NULL,
  `cdtipo` varchar(1) DEFAULT NULL,
  `vlpago` decimal(15,2) DEFAULT NULL,
  `dtpago` date DEFAULT NULL,
  `cdquem` varchar(100) DEFAULT NULL,
  `cdorig` varchar(100) DEFAULT NULL,
  `deobse` varchar(500) DEFAULT NULL,
  `flativ` varchar(15) DEFAULT NULL,
  `dtcada` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `contas`
--

INSERT INTO `contas` (`cdcont`, `decont`, `dtcont`, `vlcont`, `cdtipo`, `vlpago`, `dtpago`, `cdquem`, `cdorig`, `deobse`, `flativ`, `dtcada`) VALUES
(1, 'Consulta Médica ', '2020-04-04', '33.00', 'R', '0.00', '2020-04-04', '00000000474', '00000000272', 'Teste de Sistemas', 'S', '2020-04-04'),
(2, 'Consulta Médica Otorrino', '2020-04-04', '88.98', 'R', '0.00', '2020-04-04', '00000000474', '27145315807', 'Teste', 'S', '2020-04-04'),
(3, 'Checkup Medico ', '2020-04-04', '99.99', 'R', '0.00', '2020-04-04', '00000000272', '00000000373', 'observações\r\nobservações\r\nobservações\r\nobservações\r\nobservações\r\nobservações\r\nobservações\r\n', 'S', '2020-04-04');

-- --------------------------------------------------------

--
-- Estrutura da tabela `log`
--

CREATE TABLE `log` (
  `cdlog` bigint(20) NOT NULL,
  `delog` varchar(500) DEFAULT NULL,
  `dtlog` datetime DEFAULT NULL,
  `iplog` varchar(50) DEFAULT NULL,
  `cdusua` varchar(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `log`
--

INSERT INTO `log` (`cdlog`, `delog`, `dtlog`, `iplog`, `cdusua`) VALUES
(310, 'Acesso ao sistema efetuado nesta data', '2019-05-21 13:29:03', '45.233.14.38', '00000000191'),
(311, 'Acesso ao sistema efetuado nesta data', '2019-05-21 23:31:08', '189.6.21.76', '00000000191'),
(312, 'Acesso ao sistema efetuado nesta data', '2019-05-22 17:03:21', '138.204.242.224', '00000000191'),
(329, 'Acesso ao sistema efetuado nesta data', '2020-05-07 15:27:55', '::1', '00000000191'),
(330, '', '2020-05-07 15:28:40', '::1', '00000000191'),
(331, 'Exclusão dos dados da tabela [USUÁRIOS] para a chave [02371917000128]', '2020-05-07 15:29:01', '::1', '00000000191'),
(332, 'Acesso ao sistema efetuado nesta data', '2020-05-07 16:36:00', '::1', '00000000191'),
(333, 'Inclusão dos dados da tabela [USUÁRIOS] para [03145985027]', '2020-05-07 17:04:02', '::1', '00000000191'),
(334, 'Acesso ao sistema efetuado nesta data', '2020-05-07 17:09:19', '::1', '00000000191'),
(335, 'Acesso ao sistema efetuado nesta data', '2020-05-07 17:13:18', '::1', '00000000191'),
(336, 'Acesso ao sistema efetuado nesta data', '2020-05-07 17:14:31', '::1', '00000000191'),
(337, 'Alteração dos dados da tabela [USUÁRIOS] para a chave [00000000191]', '2020-05-07 17:18:53', '::1', '00000000191');

-- --------------------------------------------------------

--
-- Estrutura da tabela `parametros`
--

CREATE TABLE `parametros` (
  `cdpara` varchar(14) NOT NULL,
  `depara` varchar(100) DEFAULT NULL,
  `demail` varchar(255) DEFAULT NULL,
  `nrtele` varchar(20) DEFAULT NULL,
  `nrcelu` varchar(20) DEFAULT NULL,
  `deobse` varchar(500) DEFAULT NULL,
  `deende` varchar(100) DEFAULT NULL,
  `nrende` int(11) DEFAULT NULL,
  `decomp` varchar(100) DEFAULT NULL,
  `debair` varchar(100) DEFAULT NULL,
  `decida` varchar(100) DEFAULT NULL,
  `cdesta` varchar(2) DEFAULT NULL,
  `nrcepi` varchar(8) DEFAULT NULL,
  `desere` varchar(100) DEFAULT NULL,
  `demaie` varchar(255) DEFAULT NULL,
  `desene` varchar(100) DEFAULT NULL,
  `flativ` char(1) DEFAULT NULL,
  `dtcada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `parametros`
--

INSERT INTO `parametros` (`cdpara`, `depara`, `demail`, `nrtele`, `nrcelu`, `deobse`, `deende`, `nrende`, `decomp`, `debair`, `decida`, `cdesta`, `nrcepi`, `desere`, `demaie`, `desene`, `flativ`, `dtcada`) VALUES
('1234567890', 'ProntMed', 'prontmed2020@gmail.com', '(53) 123456789', '(53) 123456789', 'Sistema de Clínica Médica', 'Rua Gonçalves Chaves', 54, 'Ap', 'Centro', 'Pelotas', 'SP', '96015560', 'smartdoctorbd@gmail.com', 'smartdoctorbd@gmail.com', 'smartdoctorbd', 'S', '2017-06-14');

-- --------------------------------------------------------

--
-- Estrutura da tabela `planos`
--

CREATE TABLE `planos` (
  `cdplan` char(2) NOT NULL,
  `deplan` varchar(100) DEFAULT NULL,
  `flativ` char(1) DEFAULT NULL,
  `dtcada` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `planos`
--

INSERT INTO `planos` (`cdplan`, `deplan`, `flativ`, `dtcada`) VALUES
('01', 'IPÊ', 'S', '2020-04-15'),
('02', 'Unimed', 'S', '2017-06-15'),
('03', 'Saúde Maior', 'S', '2020-05-07'),
('04', 'Bradesco Saúde', 'S', '2017-06-15');

-- --------------------------------------------------------

--
-- Estrutura da tabela `prontuario`
--

CREATE TABLE `prontuario` (
  `cdpron` bigint(20) NOT NULL,
  `cdusua` varchar(14) DEFAULT NULL,
  `dtpron` datetime DEFAULT NULL,
  `depron` varchar(1000) DEFAULT NULL,
  `cdmedi` varchar(14) DEFAULT NULL,
  `deespe` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `prontuario`
--

INSERT INTO `prontuario` (`cdpron`, `cdusua`, `dtpron`, `depron`, `cdmedi`, `deespe`) VALUES
(1, '27145315807', '2020-05-07 13:43:16', 'Consulta para orientações gerais sobre cirurgia', '00000000474', 'Obstetrícia'),
(2, '00000000272', '2020-05-07 16:33:15', 'Consulta especializada para orientação sobre tratamento de inflamação na pâncreas com provável diagnóstico de pancreatite', '00000000474', 'Clínica Geral'),
(3, '27145315807', '2020-05-07 14:22:33', 'Teste', '00000000474', 'Clinica Geral');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `cdusua` varchar(14) NOT NULL,
  `deusua` varchar(100) DEFAULT NULL,
  `desenh` varchar(100) DEFAULT NULL,
  `demail` varchar(255) DEFAULT NULL,
  `defoto` varchar(255) DEFAULT NULL,
  `cdtipo` varchar(1) DEFAULT NULL,
  `nrtele` varchar(20) DEFAULT NULL,
  `nrcelu` varchar(20) DEFAULT NULL,
  `deobse` varchar(500) DEFAULT NULL,
  `deende` varchar(100) DEFAULT NULL,
  `nrende` int(11) DEFAULT NULL,
  `decomp` varchar(100) DEFAULT NULL,
  `debair` varchar(100) DEFAULT NULL,
  `decida` varchar(100) DEFAULT NULL,
  `cdesta` varchar(2) DEFAULT NULL,
  `nrcepi` varchar(8) DEFAULT NULL,
  `dtcada` date DEFAULT NULL,
  `flativ` char(1) DEFAULT NULL,
  `deespe` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`cdusua`, `deusua`, `desenh`, `demail`, `defoto`, `cdtipo`, `nrtele`, `nrcelu`, `deobse`, `deende`, `nrende`, `decomp`, `debair`, `decida`, `cdesta`, `nrcepi`, `dtcada`, `flativ`, `deespe`) VALUES
('00000000191', 'Administrador do Sistema I', 'e10adc3949ba59abbe56e057f20f883e', 'luiz@prontmed.com', 'img/00000000191Snapchat-522114890808426358.jpg', 'A', '(11) 1111-11111', '(22) 2-2222-2222', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '', 0, '', '', '', '', '', '2020-05-07', '', ''),
('17006678072', 'Carlos Magno', '149e9677a5989fd342ae44213df68868', 'carlosmargno@prontmed.com', 'img/semfoto.jpg', 'M', '(11) 1111-1111', '(11) 1-1111-1111', '', '', 0, '', '', '', '', '', '2020-05-07', 'S', NULL),
('44171378540', 'Paciente 44171378540', '15d4e891d784977cacbfcbb00c48f133', 'p44@p.com', 'img/semfoto.jpg', 'P', '(11) 1111-1111', '(22) 2-2222-2222', 'ASASASASA', 'Rua São Francisco', 121, '1212', 'São Geraldo', 'Juazeiro', 'BA', '48905660', '2017-09-08', 'S', '');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `agenda`
--
ALTER TABLE `agenda`
  ADD PRIMARY KEY (`cdagen`),
  ADD KEY `iagen1` (`dtagen`,`cdusua`,`cdmedi`),
  ADD KEY `iagen2` (`dtagen`,`cdmedi`,`cdusua`);

--
-- Índices para tabela `contas`
--
ALTER TABLE `contas`
  ADD PRIMARY KEY (`cdcont`),
  ADD KEY `icontas1` (`decont`,`dtcont`),
  ADD KEY `icontas2` (`dtcont`,`cdquem`),
  ADD KEY `icontas3` (`dtcont`,`cdorig`);

--
-- Índices para tabela `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`cdlog`);

--
-- Índices para tabela `parametros`
--
ALTER TABLE `parametros`
  ADD PRIMARY KEY (`cdpara`),
  ADD KEY `para1` (`depara`),
  ADD KEY `para2` (`demail`);

--
-- Índices para tabela `planos`
--
ALTER TABLE `planos`
  ADD PRIMARY KEY (`cdplan`),
  ADD KEY `planosi1` (`deplan`);

--
-- Índices para tabela `prontuario`
--
ALTER TABLE `prontuario`
  ADD PRIMARY KEY (`cdpron`),
  ADD KEY `pronti1` (`cdusua`,`dtpron`),
  ADD KEY `pronti2` (`cdmedi`,`dtpron`);

--
-- Índices para tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`cdusua`),
  ADD KEY `usuarios1` (`deusua`),
  ADD KEY `usuarios2` (`demail`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `agenda`
--
ALTER TABLE `agenda`
  MODIFY `cdagen` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de tabela `contas`
--
ALTER TABLE `contas`
  MODIFY `cdcont` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `log`
--
ALTER TABLE `log`
  MODIFY `cdlog` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=338;

--
-- AUTO_INCREMENT de tabela `prontuario`
--
ALTER TABLE `prontuario`
  MODIFY `cdpron` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
